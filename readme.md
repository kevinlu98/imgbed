# 冷文图床使用说明

## 更多内容

[冷文博客](http://www.kevinlu98.cn)

## 简介
一款以码云作为存储平台的图床工具，不限速，可快速配置，轻松入门，只需要轻松配置一下个人的码云设置，就可以让您的码云仓库成为您的私人图床，由于图片是上传掉你自己的码云仓库的，所以无需担心像一些三方图床那样出现失效的问题，只要你自己不删除，外链就一直可以使用

## 功能预览
### 外观
UI采用elementUI加typo排版，简洁大方

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/0423b425-90a7-4e9e-8bbc-108e70916a4f.jpeg](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/0423b425-90a7-4e9e-8bbc-108e70916a4f.jpeg)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/0423b425-90a7-4e9e-8bbc-108e70916a4f.jpeg)


### 支持自定义码云仓库
选择记住配置信息即可保存配置，下次打开时无需重新输入，配置信息是保存在浏览器cookie中，也无需担心信息泄露

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/b9ffc7d2-50a5-46b3-bdfe-f89958ed60cf.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/b9ffc7d2-50a5-46b3-bdfe-f89958ed60cf.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/b9ffc7d2-50a5-46b3-bdfe-f89958ed60cf.png)

### 支持预览已上传的文件
[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/e6771700-c5e5-4e8e-ac13-355a911e813f.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/e6771700-c5e5-4e8e-ac13-355a911e813f.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/e6771700-c5e5-4e8e-ac13-355a911e813f.png)

### 支持删除
[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/564df32c-ac12-4d2a-afde-40e3d09aa85f.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/564df32c-ac12-4d2a-afde-40e3d09aa85f.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/564df32c-ac12-4d2a-afde-40e3d09aa85f.png)

### 支持一建复制多种格式
[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/1ba4e7bc-da0f-43c5-8129-14e1ab920cd9.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/1ba4e7bc-da0f-43c5-8129-14e1ab920cd9.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/1ba4e7bc-da0f-43c5-8129-14e1ab920cd9.png)

### 后续更多功能待开发。。

## 配置使用

### 图床地址: 

[冷文图床](http://image.kevinlu98.cn)

### 准备一个码云仓库
- 点击个人头像旁边的加号，点击新建

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/592d88b2-1b4f-4e1f-9852-2d58d552d3d1.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/592d88b2-1b4f-4e1f-9852-2d58d552d3d1.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/592d88b2-1b4f-4e1f-9852-2d58d552d3d1.png)

- 进入创建仓库页面，填写如图的相关信息，然后点击创建


[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/c04eec40-c2cd-4001-8f5c-da752607bec3.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/c04eec40-c2cd-4001-8f5c-da752607bec3.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/c04eec40-c2cd-4001-8f5c-da752607bec3.png)

- 创建成功后选择上放的服务，然后点击GiteePages(GiteePages可以用来部署静态网站的，只需将你的静态页面上传值gitee仓库，外界就可以访问，所以我们选择这个来存储我们的图片)

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/126ede0a-691d-4c23-9e3b-80605e21929f.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/126ede0a-691d-4c23-9e3b-80605e21929f.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/126ede0a-691d-4c23-9e3b-80605e21929f.png)

- 点击启动即可

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/4e512a77-9178-49de-9965-981ca10e7d74.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/4e512a77-9178-49de-9965-981ca10e7d74.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/4e512a77-9178-49de-9965-981ca10e7d74.png)

- 开启成功

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/18151c7b-00d0-4f16-899b-a83ceda137fb.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/18151c7b-00d0-4f16-899b-a83ceda137fb.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/18151c7b-00d0-4f16-899b-a83ceda137fb.png)

- 准备工作完成

### 创建一个应用

- 点击个人头像，然后选择设置

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/4e087fac-14d6-43ba-8ede-2fe8c0a44bec.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/4e087fac-14d6-43ba-8ede-2fe8c0a44bec.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/4e087fac-14d6-43ba-8ede-2fe8c0a44bec.png)

- 在左侧菜单中找到第三方应用

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/fbd69d96-c2c7-4dba-9f78-7abe4fe17e43.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/fbd69d96-c2c7-4dba-9f78-7abe4fe17e43.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/fbd69d96-c2c7-4dba-9f78-7abe4fe17e43.png)

- 点击创建应用

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/cc1bd013-9604-445f-befb-7b0eaa2087c0.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/cc1bd013-9604-445f-befb-7b0eaa2087c0.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/cc1bd013-9604-445f-befb-7b0eaa2087c0.png)


- 按照如图填写信息

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/0c8119cf-0633-4c4a-99f9-581b8999302d.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/0c8119cf-0633-4c4a-99f9-581b8999302d.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/0c8119cf-0633-4c4a-99f9-581b8999302d.png)

- 然后跳转到应用界面  复制`Client ID`和`Client Secret`

[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/38ed0f44-300e-481e-b13b-c4aa5d1e6ec0.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/38ed0f44-300e-481e-b13b-c4aa5d1e6ec0.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/38ed0f44-300e-481e-b13b-c4aa5d1e6ec0.png)

- 现在我们需要的信息都有了

### 开始配置个人图床

- 进入[冷文图床](http://image.kevinlu98.cn/)
地址 http://image.kevinlu98.cn/

- 在[冷文图床](http://image.kevinlu98.cn/)进行如下配置


[![https://gitee.com/kevinlu98/imgbed/raw/master/20200204/e8b88c71-dcb5-499f-ad90-6e4a1b39e249.png](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/e8b88c71-dcb5-499f-ad90-6e4a1b39e249.png)](https://gitee.com/kevinlu98/imgbed/raw/master/20200204/e8b88c71-dcb5-499f-ad90-6e4a1b39e249.png)

- 现在开始你的所有操作，图片是上传掉你自己的码云仓库的，所以无需担心像一些三方图床那样出现失效的问题，只要你自己不删除，外链就一直可以使用